# Proton (sniper) SDK

This project builds Docker images based on
<https://gitlab.steamos.cloud/steamrt/sniper/sdk>, with the addition
of Proton build-dependencies.

This is mostly just a CI script, which clones
<https://github.com/ValveSoftware/Proton> and builds the same docker
images as what `make protonsdk` does there. It does a bit of magic to
create a set of commands usable by `kaniko` instead of docker, as this
CI runs on Google Compute instances.

## Using this image

Choose a suitable version number, for example 0.20230102.3-0, according
to the `PROTONSDK_VERSION` in Proton's `docker/Makefile`. Then:

    docker pull registry.gitlab.steamos.cloud/proton/sniper/sdk:0.20230102.3-0

and use that image for your builds.

At the moment the `registry.gitlab.steamos.cloud/proton/sniper/sdk:latest`
tag will change frequently and will not always point to a usable SDK image,
so please don't use that for anything.

Other Docker images whose names start with
`registry.gitlab.steamos.cloud/proton/sniper/sdk/` are built as a
side-effect of building this image, but those should not be relied on.

## Corresponding source code

Source code for all the packages that go into the Steam Runtime
base image can be found in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt-images-sniper/snapshots/> (look
in the sources/ directory).

Source code for the packages added for the Proton SDK can be found in
<https://repo.steampowered.com/proton-sdk/>.

## Information for maintainers

### Doing a test-build

To trigger a new build and publish a new test image, update the
`PROTON_COMMIT` variable in `.gitlab-ci.yml`, commit, push to
`proton/sniper/sdk` as a branch with a name other than `master`,
and wait. Using a branch named `wip/yourtopic` or `wip/yourname/yourtopic`
is suggested, replacing *yourtopic* and *yourname* as appropriate.

The image version tag that will be created is the SteamRT SDK image
version that is used as a base, as defined using the `STEAMRT_VERSION`
variable in `docker/Makefile` in the Proton source, plus the hard-coded
suffix `-dev`. If an image with the same tag already exists, it will be
overwritten.

The `PROTONSDK_VERSION` variable in `docker/Makefile` is overridden by
this project, and therefore is ignored.

### Doing a release build

When you are happy with your test-build, merge it to `master`, tag the
resulting commit and push the tag, for example:

    git tag -m "Proton SDK image v0.20230102.3-0" -a v0.20230102.3-0
    git push origin v0.20230102.3-0

A new CI pipeline will run for the git tag. This time, the Docker image
version tag that will be created is the git tag, with the "v" prefix
(if any) removed.

The tag is conventionally the same as the `PROTONSDK_VERSION` variable
in Proton's `docker/Makefile`, which is the `STEAMRT_VERSION` plus a
dash and a numeric suffix.
